package com.zaluskyi;

import com.zaluskyi.pizza.Pizza;
import com.zaluskyi.pizza.PizzaType;
import com.zaluskyi.store.LvivPizzaStore;
import com.zaluskyi.store.PizzaStore;

public class Main {
    public static void main(String[] args) {
        PizzaStore lvivPizzaStore = new LvivPizzaStore();
        Pizza pizza = lvivPizzaStore.orderPizza(PizzaType.PEPPERONI);
    }
}
