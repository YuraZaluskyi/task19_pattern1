package com.zaluskyi.pizza;

import com.zaluskyi.ingredientfactory.PizzaIngredientFactory;

public class VeggiePizza extends Pizza {
    PizzaIngredientFactory ingredientFactory;

    public VeggiePizza(PizzaIngredientFactory pizzaIngredientFactory) {
        this.ingredientFactory = ingredientFactory;
    }


    public void prepare() {
        System.out.println("Preparing " + getName());
        setDough(ingredientFactory.createDough());
        setSauce(ingredientFactory.createSauce());
        setVeggies(ingredientFactory.createVeggies());
    }
}
