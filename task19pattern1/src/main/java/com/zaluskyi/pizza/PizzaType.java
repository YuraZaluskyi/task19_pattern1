package com.zaluskyi.pizza;

public enum PizzaType {
    CHEESE, VEGGIE, CLAM, PEPPERONI
}
