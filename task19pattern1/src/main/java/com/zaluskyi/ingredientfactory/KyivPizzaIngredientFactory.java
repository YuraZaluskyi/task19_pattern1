package com.zaluskyi.ingredientfactory;

import com.zaluskyi.ingredient.Vegies.Veggies;
import com.zaluskyi.ingredient.cheese.Cheese;
import com.zaluskyi.ingredient.clam.Clam;
import com.zaluskyi.ingredient.dough.Dough;
import com.zaluskyi.ingredient.pepperoni.Pepperoni;
import com.zaluskyi.ingredient.souce.Sauce;

public class KyivPizzaIngredientFactory implements PizzaIngredientFactory {
    public Dough createDough() {
        return null;
    }

    public Sauce createSauce() {
        return null;
    }

    public Cheese createCheese() {
        return null;
    }

    public Veggies[] createVeggies() {
        return new Veggies[0];
    }

    public Pepperoni createPepperoni() {
        return null;
    }

    public Clam createClam() {
        return null;
    }
}
