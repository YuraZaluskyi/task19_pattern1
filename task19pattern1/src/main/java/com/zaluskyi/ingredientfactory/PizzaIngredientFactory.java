package com.zaluskyi.ingredientfactory;

import com.zaluskyi.ingredient.Vegies.Veggies;
import com.zaluskyi.ingredient.cheese.Cheese;
import com.zaluskyi.ingredient.clam.Clam;
import com.zaluskyi.ingredient.dough.Dough;
import com.zaluskyi.ingredient.pepperoni.Pepperoni;
import com.zaluskyi.ingredient.souce.Sauce;

public interface PizzaIngredientFactory {
    public Dough createDough();

    public Sauce createSauce();

    public Cheese createCheese();

    public Veggies[] createVeggies();

    public Pepperoni createPepperoni();

    public Clam createClam();
}
