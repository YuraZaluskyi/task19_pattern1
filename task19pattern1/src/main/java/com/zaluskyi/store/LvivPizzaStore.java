package com.zaluskyi.store;

import com.zaluskyi.pizza.*;
import com.zaluskyi.ingredientfactory.LvivPizzaIngredientFactory;
import com.zaluskyi.ingredientfactory.PizzaIngredientFactory;

public class LvivPizzaStore extends PizzaStore {
    PizzaIngredientFactory ingredientFactory = new LvivPizzaIngredientFactory();

    @Override
    public Pizza createPizza(PizzaType type) {
        Pizza pizza = null;

        if (type.equals(PizzaType.CHEESE)) {
            pizza = new CheesePizza(ingredientFactory);
            pizza.setName("Lviv style cheese pizza");
        } else if (type.equals(PizzaType.CLAM)) {
            pizza = new ClamPizza(ingredientFactory);
            pizza.setName("Lviv style calm pizza");
        } else if (type.equals(PizzaType.VEGGIE)) {
            pizza = new VeggiePizza(ingredientFactory);
            pizza.setName("Lviv style veggie pizza");
        } else if (type.equals(PizzaType.PEPPERONI)) {
            pizza = new PepperoniPizza(ingredientFactory);
            pizza.setName("Lviv style pepperoni pizza");
        }

        return pizza;
    }
}
